# Django major concepts

[//]: # (## Contents)
[//]: # (* [Routing]&#40;#routing&#41;  )
[//]: # (* [Views]&#40;#views&#41;  )
[//]: # (   * [Function-based Views]&#40;#function-based-views&#41;  )
[//]: # (   * [Class-based Views]&#40;#class-based-views&#41;  )


## Routing

The project's URLconf (`mysite/urls.py`) can point to URLconfs for individual applications.  

=== "Project URLconf"
    ```python
    # mysite/urls.py
    from django.contrib import admin
    from django.urls import include, path
    
    urlpatterns = [
        path('polls/', include('polls.urls')),
        path('snippets/', include('snippets.urls')),
        path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
        # only exception to using `include()` when including other URL patterns:
        path('admin/', admin.site.urls),
    ]
    ```
    When Django encounters `include()` in a path, it chops off whatever part of the URL matched up to that point and sends the remaining string to the included URLconf for further processing. Thus, if Django gets a request for `polls/5/results`, it will send `5/results` to `polls.urls`.  

    Since polls are in their own URLconf (`polls/urls.py`), the root path can be "/polls/" or "/fun_polls/" or "/content/polls/" or any other path root, and the app will still work.  
    Source: [Write your first view](django4_tutorial/django-tutorial-part-1.md#write-your-first-view)  
    ----

=== "Function-based view URLconf"
    ```python
    # polls/urls.py
    from django.urls import path
    
    from . import views
    
    app_name = 'polls'
    urlpatterns = [
        # ex: /polls/
        path('', views.index, name='index'),
        # ex: /polls/5/
        path('<int:question_id>/', views.detail, name='detail'),
        # ex: /polls/5/results/
        path('<int:question_id>/results/', views.results, name='results'),
        # ex: /polls/5/vote/
        path('<int:question_id>/vote/', views.vote, name='vote'),
    ]
    ```
    The `path()` function takes a route (req), view (req), kwargs (opt), and name (opt).  

    * `route`: contains a URL pattern; finds the first one that matches
    * `view`: Django calls the view function with a `HttpRequest` object as the first argument and any "captured" values from the route as keyword arguments
    * `kwargs`: arbitrary keyword arguments in a dict
    * `name`: "Naming your URL lets you refer to it unambiguously from elsewhere in Django, especially from within templates." 
    Source: [Write your first view](django4_tutorial/django-tutorial-part-1.md#write-your-first-view)  

    The route can contain a pattern in angle brackets (`#!python {'Pattern': <class 'django.urls.resolvers.RoutePattern'>}`) of the form `<converter:pattern name>`, and that pattern name is sent to a view as an argument. Specifically, in the `WSGIRequest` object sent to the view, we'll see something like: `captured_kwargs = {'question_id': 34}`. Or, if calling the `detail()` view: `#!python detail(request=<HttpRequest object>, question_id=34)`.  
    Source: [Writing more views](django4_tutorial/django-tutorial-part-3.md#writing-more-views)  
    ----

=== "Class-based view URLconf"
    ```python
    # polls/urls.py
    from django.urls import path
    
    from . import views
    
    app_name = 'polls'
    urlpatterns = [
        path('', views.IndexView.as_view(), name='index'),
        path('<int:pk>/', views.DetailView.as_view(), name='detail'),
        path('<int:pk>/results/', views.ResultsView.as_view(), name='results'),
        path('<int:question_id>/vote/', views.vote, name='vote'),
    ]
    ```

    ----

## Views

### Function-based Views
The following URL pattern from `polls/urls.py` points to a simple index view:  
```python
urlpatterns = [
    path('', views.index, name='index'),
]
```
The view code is shown in "Minimal" below. Changed lines are highlighted as the views are changed throughout the tutorial.

=== "Minimal"
    ```python linenums="1"
    # polls/views.py
    from django.http import HttpResponse


    def index(request):
        return HttpResponse("Hello, world. You're at the polls index.")
    
    def detail(request, question_id):
        return HttpResponse("You're looking at question %s." % question_id)
    
    def results(request, question_id):
        response = "You're looking at the results of question %s."
        return HttpResponse(response % question_id)
    
    def vote(request, question_id):
        return HttpResponse("You're voting on question %s." % question_id)
    ```
    Source: [Write your first view](django4_tutorial/django-tutorial-part-1.md#write-your-first-view)  

    A **view** "is a 'type' of web page in a Django application that serves a specific function and has a specific template". The URLconf tells Django which view to use for each URL. Each view is represented by a Python function (note the `index()`, `detail()` and other functions). It returns an `HttpResponse` object or an exception (like `Http404`). They can do anything else in-between.  
    Source: [Overview](django4_tutorial/django-tutorial-part-3.md#overview)  
    Source: [Write views that actually do something](django4_tutorial/django-tutorial-part-3.md#write-views-that-actually-do-something)  

    The `question_id` parameter comes from the URLconf: `path('<int:question_id>/', views.detail, name='detail')`.  
    Source: [Writing more views](django4_tutorial/django-tutorial-part-3.md#writing-more-views)  
    ----

=== "Return 5 objects"
    ```python linenums="1" hl_lines="4 8-10"
    # polls/views.py
    from django.http import HttpResponse
    
    from .models import Question
    
    
    def index(request):
        latest_question_list = Question.objects.order_by('-pub_date')[:5]
        output = ', '.join([q.question_text for q in latest_question_list])
        return HttpResponse(output)

    def detail(request, question_id):
        return HttpResponse("You're looking at question %s." % question_id)
    
    def results(request, question_id):
        response = "You're looking at the results of question %s."
        return HttpResponse(response % question_id)
    
    def vote(request, question_id):
        return HttpResponse("You're voting on question %s." % question_id)
    ```

    ----

=== "Template"
    ```python linenums="1" hl_lines="3 10-12"
    # polls/views.py
    from django.http import HttpResponse
    from django.template import loader
    
    from .models import Question
    
    
    def index(request):
        latest_question_list = Question.objects.order_by('-pub_date')[:5]
        template = loader.get_template('polls/index.html')
        context = {'latest_question_list': latest_question_list}
        return HttpResponse(template.render(context, request))

    def detail(request, question_id):
        return HttpResponse("You're looking at question %s." % question_id)
    
    def results(request, question_id):
        response = "You're looking at the results of question %s."
        return HttpResponse(response % question_id)
    
    def vote(request, question_id):
        return HttpResponse("You're voting on question %s." % question_id)
    ```
    Note that we should create the template at `mysite/polls/templates/polls/index.html` and reference it as `polls/index.html`. I think this is because Django will choose the first template it finds whose name matches, and we're likely to have an `index.html` in another application, too (hence the `templates/polls/` subdirectory).

    Source: [Write views that actually do something](django4_tutorial/django-tutorial-part-3.md#write-views-that-actually-do-something)  
    ----

=== "`render()` shortcut"
    ```python linenums="1" hl_lines="3 11"
    # polls/views.py
    from django.http import HttpResponse
    from django.shortcuts import render
    
    from .models import Question
    
    
    def index(request):
        latest_question_list = Question.objects.order_by('-pub_date')[:5]
        context = {'latest_question_list': latest_question_list}
        return render(request, 'polls/index.html', context)

    def detail(request, question_id):
        return HttpResponse("You're looking at question %s." % question_id)
    
    def results(request, question_id):
        response = "You're looking at the results of question %s."
        return HttpResponse(response % question_id)
    
    def vote(request, question_id):
        return HttpResponse("You're voting on question %s." % question_id)
    ```
    The view can be simplified by using the `render()` shortcut. 
    Source: [Write views that actually do something](django4_tutorial/django-tutorial-part-3.md#write-views-that-actually-do-something)  
    ----

=== "Raise a 404"
    ```python linenums="1" hl_lines="2 14-18"
    # polls/views.py
    from django.http import Http404, HttpResponse
    from django.shortcuts import render
    
    from .models import Question


    def index(request):
        latest_question_list = Question.objects.order_by('-pub_date')[:5]
        context = {'latest_question_list': latest_question_list}
        return render(request, 'polls/index.html', context)

    def detail(request, question_id):
        try:
            question = Question.objects.get(pk=question_id)
        except Question.DoesNotExist:
            raise Http404("Question does not exist")
        return render(request, 'polls/detail.html', {'question': question})
    
    def results(request, question_id):
        response = "You're looking at the results of question %s."
        return HttpResponse(response % question_id)
    
    def vote(request, question_id):
        return HttpResponse("You're voting on question %s." % question_id)
    ```

    ----

=== "`get_object_or_404()` shortcut"
    ```python linenums="1" hl_lines="3 14-15"
    # polls/views.py
    from django.http import Http404, HttpResponse
    from django.shortcuts import get_object_or_404, render
    
    from .models import Question


    def index(request):
        latest_question_list = Question.objects.order_by('-pub_date')[:5]
        context = {'latest_question_list': latest_question_list}
        return render(request, 'polls/index.html', context)

    def detail(request, question_id):
        question = get_object_or_404(Question, pk=question_id)
        return render(request, 'polls/detail.html', {'question': question})
 
    def results(request, question_id):
        response = "You're looking at the results of question %s."
        return HttpResponse(response % question_id)
    
    def vote(request, question_id):
        return HttpResponse("You're voting on question %s." % question_id)
    ```
    We could raise a 404 error if the requested object is not found. See the "Raise a 404" view. There's a shortcut for that, too. See the "`get_object_or_404()` shortcut" view.    
    Source: [Raising a 404 error](django4_tutorial/django-tutorial-part-3.md#raising-a-404-error)  
    ----

=== "POST handling"
    ```python linenums="1" hl_lines="2 4 23-38"
    # polls/views.py
    from django.http import HttpResponse, HttpResponseRedirect
    from django.shortcuts import get_object_or_404, render
    from django.urls import reverse
    
    from .models import Choice, Question


    def index(request):
        latest_question_list = Question.objects.order_by('-pub_date')[:5]
        context = {'latest_question_list': latest_question_list}
        return render(request, 'polls/index.html', context)

    def detail(request, question_id):
        question = get_object_or_404(Question, pk=question_id)
        return render(request, 'polls/detail.html', {'question': question})

    def results(request, question_id):
        question = get_object_or_404(Question, pk=question_id)
        return render(request, 'polls/results.html', {'question': question})

    def vote(request, question_id):
        question = get_object_or_404(Question, pk=question_id)
        try:
            selected_choice = question.choice_set.get(pk=request.POST['choice'])
        except (KeyError, Choice.DoesNotExist):
            # Redisplay the question voting form.
            return render(request, 'polls/detail.html', {
                'question': question,
                'error_message': "You didn't select a choice.",
            })
        else:
            selected_choice.votes += 1
            selected_choice.save()
            # Always return an HttpResponseRedirect after successfully dealing
            # with POST data. This prevents data from being posted twice if a
            # user hits the Back button.
            return HttpResponseRedirect(reverse('polls:results', args=(question.id,)))
    ```
    Modify the `vote` view to handle the `detail.html` form submission to the `polls/<int:question_id>/vote` endpoint.  
    The `vote` view has a few new items:  

    * `request.POST` is a dictionary-like object that lets you access submitted data by key name
        * `request.POST` values are always strings
        * `request.GET` can access data in the same way
    * `HttpResponseRedirect` takes a single argument: the URL to which the user will be redirected
    * `reverse()` helps avoid having to hardcode a URL in the view function
        * input: name of the view that we want to pass control to and the variable portion of the URL pattern that points to that view
        * output: `'/polls/3/results/'` (`3` is the value of `question.id`)  

    Note that the `vote` view could result in race conditions when updating the vote count.    
    Source: [Write a minimal form](django4_tutorial/django-tutorial-part-4.md#write-a-minimal-form)  
    ----

All four shortcuts:  

* `render()`
* `redirect()`
* `get_object_or_404()`
* `get_list_or_404()`

### Class-based Views
Generic views abstract common patterns for less redundant code. Generally, one would use generic views from the beginning if they are a good fit, but since the tutorial involves refactoring existing views, there are steps:  

1. Convert the URLconf
2. Delete old, unneeded views
3. Introduce new views based on generic views

Here is the new view file:  
```python
# polls/views.py
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from django.views import generic

from .models import Choice, Question


class IndexView(generic.ListView):
    template_name = 'polls/index.html'
    context_object_name = 'latest_question_list'

    def get_queryset(self):
        """Return the last five published questions."""
        return Question.objects.order_by('-pub_date')[:5]


class DetailView(generic.DetailView):
    model = Question
    template_name = 'polls/detail.html'


class ResultsView(generic.DetailView):
    model = Question
    template_name = 'polls/results.html'


def vote(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    try:
        selected_choice = question.choice_set.get(pk=request.POST['choice'])
    except (KeyError, Choice.DoesNotExist):
        # Redisplay the question voting form.
        return render(request, 'polls/detail.html', {
            'question': question,
            'error_message': "You didn't select a choice.",
        })
    else:
        selected_choice.votes += 1
        selected_choice.save()
        # Always return an HttpResponseRedirect after successfully dealing
        # with POST data. This prevents data from being posted twice if a
        # user hits the Back button.
        return HttpResponseRedirect(reverse('polls:results', args=(question.id,)))
```

Notes on `DetailView`:  

* `DetailView` expects the primary key value from the URL to be called `pk`, so we had to change `question_id` to `pk` in `polls/urls.py`  
* ? Why not do that for the `vote` URL, too?  
* by default, uses a template called `<app name>/<model name>_detail.html` (`polls/question_detail.html`) -
* override default template using `template_name` attribute
* the `question` variable is automatically provided to our context

Notes on `ListView`:  

* uses a default template of `<app name>/<model name>_list.html`
* by default, the context variable would be `question_list` so we specify the `context_object_name` attribute to tell Django we want to use `latest_question_list` instead (because that is the context name we have been using in the templates)

Source: [Use generic views: Less code is better](django4_tutorial/django-tutorial-part-4.md#use-generic-views-less-code-is-better)  


https://docs.djangoproject.com/en/4.1/topics/class-based-views/intro/
"Because Django’s URL resolver expects to send the request and associated arguments to a callable function, not a class, class-based views have an `as_view()` class method which returns a function that can be called when a request arrives for a URL matching the associated pattern. The function creates an instance of the class, calls `setup()` to initialize its attributes, and then calls its `dispatch()` method. `dispatch` looks at the request to determine whether it is a `GET`, `POST`, etc, and relays the request to a matching method if one is defined, or raises `HttpResponseNotAllowed` if not....It is worth noting that what your method returns is identical to what you return from a function-based view, namely some form of `HttpResponse`. This means that http shortcuts or `TemplateResponse` objects are valid to use inside a class-based view."



Class-based attributes can be set by subclassing and overriding attributes and methods or by passing them as keyword arguments to the `as_view()` method in the URLconf.  

"Note also that you can only inherit from one generic view - that is, only one parent class may inherit from `View` and the rest (if any) should be mixins."

? "This approach applies the decorator on a per-instance basis. If you want every instance of a view to be decorated, you need to take a different approach." Per-instance vs. every instance?  

[`model`](https://docs.djangoproject.com/en/4.1/ref/class-based-views/mixins-single-object/#django.views.generic.detail.SingleObjectMixin.model) on view: "Specifying `model = Foo` is effectively the same as specifying `queryset = Foo.objects.all()`, where `objects` stands for `Foo`’s default manager."  




## Forms
Here is a minimal form for POST requests (see `mysite/polls/templates/polls/detail.html`):  
```html
<form action="{% url 'polls:vote' question.id %}" method="post">
{% csrf_token %}
<fieldset>
    <legend><h1>{{ question.question_text }}</h1></legend>
    {% if error_message %}<p><strong>{{ error_message }}</strong></p>{% endif %}
    {% for choice in question.choice_set.all %}
        <input type="radio" name="choice" id="choice{{ forloop.counter }}" value="{{ choice.id }}">
        <label for="choice{{ forloop.counter }}">{{ choice.choice_text }}</label><br>
    {% endfor %}
</fieldset>
<input type="submit" value="Vote">
</form>
```

* the `detail` form template displays a radio button for each question choice
   * the value of each radio button is the associated question choice’s ID
   * the name of each radio button is "choice"
   * that means, when somebody selects one of the radio buttons and submits the form, it’ll send the POST data `choice=#` where # is the ID of the selected choice
   * this is the basic concept of HTML forms  
* we set the form’s action to `{% url 'polls:vote' question.id %}`, and we set `method="post"`  
* `forloop.counter` indicates how many times the `for` tag has gone through its loop
* the `{% csrf_token %}` template tag takes care of Cross Site Request Forgeries; it's on the rendered page as `<input type="hidden" name="csrfmiddlewaretoken" value="sQPhBBvcgNGOdJmVkKimPry2defr0SUl1xHo8JgUxL6q4IMDNvk1YR6hDen9gFG9">`

Source: [Write a minimal form](django4_tutorial/django-tutorial-part-4.md#write-a-minimal-form)  

