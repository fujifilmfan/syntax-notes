# Django Tutorial Part 3
[Writing your first Django app, part 3](https://docs.djangoproject.com/en/4.1/intro/tutorial03/)

## Contents
* [Overview](#overview)  
* [Writing more views](#writing-more-views)  
* [Write views that actually do something](#write-views-that-actually-do-something)  
* [Raising a 404 error](#raising-a-404-error)  
* [Use the template system](#use-the-template-system)  
* [Removing hardcoded URLs in templates](#removing-hardcoded-urls-in-templates)  
* [Namespacing URL names](#namespacing-url-names)  

## Overview  
Terminology: a **view** "is a 'type' of web page in your Django application that generally serves a specific function and has a specific template" (could be a comment action, for instance)  

The Poll application will have:  

1. Question “index” page: displays the latest few questions.  
2. Question “detail” page: displays a question text, with no results but with a form to vote.  
3. Question “results” page: displays results for a particular question.  
4. Vote action: handles voting for a particular choice in a particular question.  

In Django, web pages and content are delivered by views. Each view is represented by a Python function. The view is chosen based on the URL that's requested, and 'URLconfs' are used to map URL patterns to views.  

## Writing more views  
Add views that take `question_id` as an argument.  

? Why does the Django tutorial use % formatting for `HttpResponse`?  

`'/polls/<int:question_id>/results/'` would be like `'/polls/{question_id}/results'` in FastAPI, I think.  Our `settings.ROOT_URLCONF` points to `mysite.urls` where Django finds `urlpatterns` and traverses the patterns in order.  

"Using angle brackets 'captures' part of the URL and sends it as a keyword argument to the view function." <int:question_id> => <converter:pattern name>  
In the `WSGIRequest` object sent to the view: `captured_kwargs = {'question_id': 34}`  

## Write views that actually do something  
A view returns an `HttpResponse` object or an exception (like `Http404`). Do anything else in-between.  

The new `index` view would give a format like:  

```
* Should we hire her?
* What's new?
```

with two questions in polls.  

"Your project’s `TEMPLATES` setting describes how Django will load and render templates. The default settings file configures a `DjangoTemplates` backend whose `APP_DIRS` option is set to `True`. By convention `DjangoTemplates` looks for a 'templates' subdirectory in each of the `INSTALLED_APPS`."  

`{% endif %}` <-- cute, it's like bash's `fi`, but what of `{% endfor %}`?  

Create `mysite/polls/templates/polls/index.html`, but this can be referred to as `polls/index.html`. :thinking_face: This is done b/c Django will choose the first template it finds whose name matches, and I guess we're likely to have an `index.html` in another application, too.  

We can use `render()` to render our template and return it to the caller.  

## Raising a 404 error  
We can use `Http404` to raise a 404 error if a question does not exist (for instance).  

We can use `get_object_or_404()` to automatically raise a 404 error if the object does not exist.  

We can use `Http404` or `get_object_or_404`. "The `get_object_or_404()` function takes a Django model as its first argument and an arbitrary number of keyword arguments, which it passes to the `get()` function of the model’s manager. It raises `Http404` if the object doesn't exist."  

There is also a `get_list_or_4o4()` function that uses `filter()` instead of `get()`.  

## Use the template system  
For `{{ question.question_text }}`, Django first tries a dictionary lookup on the `question` object (fails), then an attribute lookup (succeeds), then if that fails, a list index lookup.  

## Removing hardcoded URLs in templates  
Replace  
`<li><a href="/polls/{{ question.id }}/">{{ question.question_text }}</a></li>`  
with  
`<li><a href="{% url 'detail' question.id %}">{{ question.question_text }}</a></li>`  

Then, URL changes only need to be made in `urls.py`, like from:  
`path('<int:question_id>/', views.detail, name='detail'),`  
to:  
`path('specifics/<int:question_id>/', views.detail, name='detail'),`  

## Namespacing URL names  
Add an `app_name` to `polls/urls.py` and prefix the view name in the template with `app_name:`, like `polls:detail`. This tells Django which app view to use (since more than one app might have a `detail` view, for instance).  
