# Django Tutorial Part 4
[Writing your first Django app, part 4](https://docs.djangoproject.com/en/4.1/intro/tutorial04/)

## Contents
* [Write a minimal form](#write-a-minimal-form)  
* [Use generic views: Less code is better](#use-generic-views-less-code-is-better)  

## Write a minimal form
See `mysite/polls/templates/polls/detail.html`.

* the `detail` template displays a radio button for each question choice
   * the value of each radio button is the associated question choice’s ID
   * the name of each radio button is "choice"
   * that means, when somebody selects one of the radio buttons and submits the form, it’ll send the POST data `choice=#` where # is the ID of the selected choice
   * this is the basic concept of HTML forms  
* We set the form’s action to `{% url 'polls:vote' question.id %}`, and we set `method="post"`  
* `forloop.counter` indicates how many times the `for` tag has gone through its loop
* the `{% csrf_token %}` template tag takes care of Cross Site Request Forgeries; it's on the rendered page as `<input type="hidden" name="csrfmiddlewaretoken" value="sQPhBBvcgNGOdJmVkKimPry2defr0SUl1xHo8JgUxL6q4IMDNvk1YR6hDen9gFG9">`

The `vote` view has a few new items.

* `request.POST` is a dictionary-like object that lets you access submitted data by key name
   * `request.POST` values are always strings
   * `request.GET` can access data in the same way
* `HttpResponseRedirect` takes a single argument: the URL to which the user will be redirected
* `reverse()` helps avoid having to hardcode a URL in the view function
  * input: name of the view that we want to pass control to and the variable portion of the URL pattern that points to that view
  * output: `'/polls/3/results/'` (`3` is the value of `question.id`)

Note that the `vote` view could result in race conditions when updating the vote count.  

Add a `polls/results.html` template.  

## Use generic views: Less code is better
Notes on `DetailView`:  

* `DetailView` expects the primary key value from the URL to be called `pk`, so we had to change `question_id` to `pk` in `polls/urls.py`  
* ? Why not do that for the `vote` URL, too?  
* by default, uses a template called `<app name>/<model name>_detail.html` (`polls/question_detail.html`) -
* override default template using `template_name` attribute
* the `question` variable is automatically provided to our context

Notes on `ListView`:  

* uses a default template of `<app name>/<model name>_list.html`
* by default, the context variable would be `question_list` so we specify the `context_object_name` attribute to tell Django we want to use `latest_question_list` instead
