# Django Tutorial Part 1
[Writing your first Django app, part 1](https://docs.djangoproject.com/en/4.1/intro/tutorial01/)  
## Contents
* [Creating a project](#creating-a-project)  
* [The development server](#the-development-server)  
* [Creating the Polls app](#creating-the-polls-app)  
* [Write your first view](#write-your-first-view)  

## Creating a project
```shell
django-admin startproject mysite
```

Creates the following directory structure:  
```
mysite/
    manage.py
    mysite/
        __init__.py
        settings.py
        urls.py
        asgi.py
        wsgi.py
```

The files:  

* The outer `mysite/` root directory is a container for your project. Its name doesn't matter to Django; you can rename it to anything you like.  
* `manage.py`: A command-line utility that lets you interact with this Django project in various ways.  
* The inner `mysite/` directory is the actual Python package for your project. Its name is the Python package name you’ll need to use to import anything inside it (e.g. `mysite.urls`).  
* `mysite/__init__.py`: An empty file that tells Python that this directory should be considered a Python package.  
* `mysite/settings.py`: Settings/configuration for this Django project.  
* `mysite/urls.py`: The URL declarations for this Django project; a “table of contents” of your Django-powered site.  
* `mysite/asgi.py`: An entry-point for ASGI-compatible web servers to serve your project.  
* `mysite/wsgi.py`: An entry-point for WSGI-compatible web servers to serve your project.  

## The development server

```shell
django-4/mysite > python manage.py runserver
```

The app will be running at http://127.0.0.1:8000/. The IP address and port can be changed.  

```shell
# change port
python manage.py runserver 8080
# change IP address
python manage.py runserver 0:8000
```

Feature:  

* the development server automatically reloads Python code for each request as needed (exceptions: adding files)  

## Creating the Polls app
Terminology:  

* **app**: web application that does something – e.g., a blog system, a database of public records or a small poll app
* **project**: collection of configuration and apps for a particular website  

"A project can contain multiple apps. An app can be in multiple projects."  

```shell
python manage.py startapp polls
```

Creates the following directory structure:  

```
polls/
    __init__.py
    admin.py
    apps.py
    migrations/
        __init__.py
    models.py
    tests.py
    views.py
```

## Write your first view
1. Create a view (`index()` in `views.py`)
2. Create a `polls/urls.py` file
3. Add the `path('', views.index, name='index')` URL pattern in `polls/urls.py`
4. Point the root URLconf in `mysite/urls.py` to `polls.urls` module by adding `path('polls/', include('polls.urls'))`  

"Whenever Django encounters `include()`, it chops off whatever part of the URL matched up to that point and sends the remaining string to the included URLconf for further processing....Since polls are in their own URLconf (`polls/urls.py`), they can be placed under “/polls/”, or under “/fun_polls/”, or under “/content/polls/”, or any other path root, and the app will still work."  

The `path()` function takes a route (req), view (req), kwargs (opt), name (opt), and Pattern (?) ({'Pattern': <class 'django.urls.resolvers.RoutePattern'>}).  
* `route`: contains a URL pattern; finds the first one that matches
* `view`: Django calls the view function with a `HttpRequest` object as the first argument and any "captured" values from the route as keyword arguments
* `kwargs`: arbitrary keyword arguments in a dict
* `name`: "Naming your URL lets you refer to it unambiguously from elsewhere in Django, especially from within templates."  
