# Django Tutorial Part 2
[Writing your first Django app, part 2](https://docs.djangoproject.com/en/4.1/intro/tutorial02/)  

## Contents
* [Database setup](#database-setup)  
* [Creating models](#creating-models)  
* [Activating models](#activating-models)  
* [Playing with the API](#playing-with-the-api)  
* [Introducing the Django Admin](#introducing-the-django-admin)  

## Database setup
`SQLite` is used by default because it comes with Python.  

Default installed apps in `settings.py`:  

* `django.contrib.admin`: The admin site. You’ll use it shortly.  
* `django.contrib.auth`: An authentication system.  
* `django.contrib.contenttypes`: A framework for content types.  
* `django.contrib.sessions`: A session framework.  
* `django.contrib.messages`: A messaging framework.  
* `django.contrib.staticfiles`: A framework for managing static files.  

Un-needed apps can be commented-out or deleted. Some of these apps rely on a database, so we can create the tables with:  

```shell
python manage.py migrate
```

The `migrate` command will only run migrations for apps in `INSTALLED_APPS`.  

## Creating models
Models are essentially your database layout. Each class variable represents a database field. Names like `question_text` and `pub_date` are field names in machine-friendly format (they will also be the database column names). An optional first positional argument can be used for a human-readable name (like with `date published`.    

## Activating models
The model allows Django to create a database schema and create a Python database-access API.  

Add `polls.apps.PollsConfig` to `mysite/settings.py`.  

Terminology: a **migration** stores changes to models  

Make migrations since we have added new models:  

```shell
python manage.py makemigrations polls
```

View the SQL that a migration would run:  

```shell
python manage.py sqlmigrate polls 0001
```

* the `django_migrations` table tracks which migrations have already been applied
* the exact output depends on the database  
* tables names are generated using the app name and model name, like `polls_choice` (I think we override this behavior at Disney)  
* primary keys are added automatically  
* Django appends `_id` to the foreign key field name  
* foreign key relationship is made explicit by a `FOREIGN KEY` constraint  

You can also run `python manage.py check` to check for problems without making migrations or touching the database.  

Remember the sequence:  

1. change models in `models.py`  
2. run `python manage.py makemigrations` to create migrations for changes  
3. run `python manage.py migrate` to apply changes  

## Playing with the API
```shell
python manage.py shell
```

```python
### API
>>> from polls.models import Choice, Question  # Import the model classes we just wrote.

# No questions are in the system yet.
>>> Question.objects.all()
<QuerySet []>

# Create a new Question.
# Support for time zones is enabled in the default settings file, so
# Django expects a datetime with tzinfo for pub_date. Use timezone.now()
# instead of datetime.datetime.now() and it will do the right thing.
>>> from django.utils import timezone
>>> q = Question(question_text="What's new?", pub_date=timezone.now())

# Save the object into the database. You have to call save() explicitly.
>>> q.save()

# Now it has an ID.
>>> q.id
1

# Access model field values via Python attributes.
>>> q.question_text
"What's new?"
>>> q.pub_date
datetime.datetime(2012, 2, 26, 13, 0, 0, 775217, tzinfo=<UTC>)

# Change values by changing the attributes, then calling save().
>>> q.question_text = "What's up?"
>>> q.save()

# objects.all() displays all the questions in the database.
>>> Question.objects.all()
<QuerySet [<Question: Question object (1)>]>
```

Add `__str__` methods to the models to get more useful output from `Question.objects.all()`.  

* "It’s important to add `__str__()` methods to your models, not only for your own convenience when dealing with the interactive prompt, but also because objects’ representations are used throughout Django’s automatically-generated admin."  
* instead of `<QuerySet [<Question: Question object (1)>]>`, we'll get `<QuerySet [<Question: What's up?>]>`; much better  

Add a `was_published_recently()` method to the `Question` class.  

```python
>>> from polls.models import Choice, Question

# Make sure our __str__() addition worked.
>>> Question.objects.all()
<QuerySet [<Question: What's up?>]>

# Django provides a rich database lookup API that's entirely driven by
# keyword arguments.
>>> Question.objects.filter(id=1)
<QuerySet [<Question: What's up?>]>
>>> Question.objects.filter(question_text__startswith='What')
<QuerySet [<Question: What's up?>]>

# Get the question that was published this year.
>>> from django.utils import timezone
>>> current_year = timezone.now().year
>>> Question.objects.get(pub_date__year=current_year)
<Question: What's up?>

# Request an ID that doesn't exist, this will raise an exception.
>>> Question.objects.get(id=2)
Traceback (most recent call last):
    ...
DoesNotExist: Question matching query does not exist.

# Lookup by a primary key is the most common case, so Django provides a
# shortcut for primary-key exact lookups.
# The following is identical to Question.objects.get(id=1).
>>> Question.objects.get(pk=1)
<Question: What's up?>

# Make sure our custom method worked.
>>> q = Question.objects.get(pk=1)
>>> q.was_published_recently()
True

# Give the Question a couple of Choices. The create call constructs a new
# Choice object, does the INSERT statement, adds the choice to the set
# of available choices and returns the new Choice object. Django creates
# a set to hold the "other side" of a ForeignKey relation
# (e.g. a question's choice) which can be accessed via the API.
>>> q = Question.objects.get(pk=1)

# Display any choices from the related object set -- none so far.
>>> q.choice_set.all()
<QuerySet []>

# Create three choices.
>>> q.choice_set.create(choice_text='Not much', votes=0)
<Choice: Not much>
>>> q.choice_set.create(choice_text='The sky', votes=0)
<Choice: The sky>
>>> c = q.choice_set.create(choice_text='Just hacking again', votes=0)

# Choice objects have API access to their related Question objects.
>>> c.question
<Question: What's up?>

# And vice versa: Question objects get access to Choice objects.
>>> q.choice_set.all()
<QuerySet [<Choice: Not much>, <Choice: The sky>, <Choice: Just hacking again>]>
>>> q.choice_set.count()
3

# The API automatically follows relationships as far as you need.
# Use double underscores to separate relationships.
# This works as many levels deep as you want; there's no limit.
# Find all Choices for any question whose pub_date is in this year
# (reusing the 'current_year' variable we created above).
>>> Choice.objects.filter(question__pub_date__year=current_year)
<QuerySet [<Choice: Not much>, <Choice: The sky>, <Choice: Just hacking again>]>

# Let's delete one of the choices. Use delete() for that.
>>> c = q.choice_set.filter(choice_text__startswith='Just hacking')
>>> c.delete()
```

## Introducing the Django Admin
Create a user who can login to the admin site:  
```shell
> python manage.py createsuperuser
Username (leave blank to use 'kurt.klein'): admin
Email address: kurtklein@hey.com
Password: 
Password (again): 
The password is too similar to the username.
This password is too common.
Bypass password validation and create user anyway? [y/N]: y
Superuser created successfully.
```

The Groups and Users in the admin site are provided by `django.contrib.auth`, the authentication framework shipped by Django.  

For `Question` objects to show on the admin interface, register the model in `polls/admin.py`.  

The Question form is automatically generated from the `Question` model

Changing items is easy to do in the Django admin - seems dangerous!  
