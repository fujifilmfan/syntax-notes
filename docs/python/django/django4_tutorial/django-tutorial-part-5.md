# Django Tutorial Part 5
[Writing your first Django app, part 5](https://docs.djangoproject.com/en/4.1/intro/tutorial05/)

## Contents
* [Introducing automated testing](#introducing-automated-testing)  
* [Writing our first test](#writing-our-first-test)  
  * [We identify a bug](#we-identify-a-bug)  
  * [Create a test to expose the bug](#create-a-test-to-expose-the-bug)  
  * [Running tests](#running-tests)  
  * [Fixing the bug](#fixing-the-bug)  
  * [More comprehensive tests](#more-comprehensive-tests)  
* [Test a view](#test-a-view)  
  * [A test for a view](#a-test-for-a-view)  
  * [The Django test client](#the-django-test-client)  
  * [Improving our view](#improving-our-view)  
  * [Testing our new view](#testing-our-new-view)  
  * [Testing the DetailView](#testing-the-detailview)  
  * [Ideas for more tests](#ideas-for-more-tests)  
* [When testing, more is better](#when-testing-more-is-better)  
* [Further testing](#further-testing)  


## Introducing automated testing
Why test?  

* Tests will save you time  
* Tests don’t just identify problems, they prevent them  
* Tests make your code more attractive  
* Tests help teams work together  

## Writing our first test

### We identify a bug
Look, our app thinks future questions are published recently!  

```python
>>> import datetime
>>> from django.utils import timezone
>>> from polls.models import Question
>>> # create a Question instance with pub_date 30 days in the future
>>> future_question = Question(pub_date=timezone.now() + datetime.timedelta(days=30))
>>> # was it published recently?
>>> future_question.was_published_recently()
True
```

### Create a test to expose the bug
The test runner will find tests in any file whose name begins with `test`.
See `mysite/polls/tests.py`.  I added a few extra tests while I was at it.  

**The database is reset for each test method.**  

### Running tests
`django-4/mysite > python manage.py test polls`  

This can be enhanced a variety of ways, including:  
`python manage.py test polls --keepdb --parallel --verbosity 3`  

The `--parallel` flag did not work with view tests:  
```shell
    raise AppRegistryNotReady("Apps aren't loaded yet.")
django.core.exceptions.AppRegistryNotReady: Apps aren't loaded yet.
```

## Test a view
> The polls application is fairly undiscriminating: it will publish any question, including ones whose `pub_date` field lies in the future. We should improve this. Setting a `pub_date` in the future should mean that the Question is published at that moment, but invisible until then.  

### A test for a view
Instead of testing the internal behavior of the code, the view test will test the application's behavior as it would be experienced by a user through a web browser.  

### The Django test client
The test `Client` simulates a user interacting with the code at the view level. It can be used in `tests.py` and in the `shell`. In `tests.py`, the `django.test.TestCase` class comes with its own client.  

In the shell, run `setup_test_environment()` to install a template renderer that also allows us to see `response.context` and other attributes we couldn't otherwise. This method uses the existing database.
```python
>>> from django.test.utils import setup_test_environment
>>> setup_test_environment()
```

Import the test client class:  
```python
>>> from django.test import Client
>>> client = Client()
```

Do some work:  
```python
>>> # get a response from '/'
>>> response = client.get('/')
Not Found: /
>>> # we should expect a 404 from that address; if you instead see an
>>> # "Invalid HTTP_HOST header" error and a 400 response, you probably
>>> # omitted the setup_test_environment() call described earlier.
>>> response.status_code
404
>>> # on the other hand we should expect to find something at '/polls/'
>>> # we'll use 'reverse()' rather than a hardcoded URL
>>> from django.urls import reverse
>>> response = client.get(reverse('polls:index'))
>>> response.status_code
200
>>> response.content
b'\n    <ul>\n    \n        <li><a href="/polls/1/">What&#x27;s up?</a></li>\n    \n    </ul>\n\n'
>>> response.context['latest_question_list']
<QuerySet [<Question: What's up?>]>
```

The 400 message would look like: "django.core.exceptions.DisallowedHost: Invalid HTTP_HOST header: 'testserver'. You may need to add 'testserver' to ALLOWED_HOSTS."  

### Improving our view
Add a lte `filter` to the `get_queryset()` method in `IndexView` to only retrieve questions published earlier than now.  

### Testing our new view
Add a `create_question()` helper function and `QuestionIndexViewTests` class.  

### Testing the DetailView
We need to change the `DetailView` to not show future questions (such as if a user knows or guesses the URL). Adding the lte filter results in a 404 to `polls/2/` (my future question).  

### Ideas for more tests

* add a `get_queryset()` method to `ResultsView` and test it  
* ensure views check for `Choices` when `Questions` are published (which would break some existing tests!)  

## When testing, more is better
Rules of thumb:  

* a separate `TestClass` for each model or view  
* a separate test method for each set of conditions you want to test  
* test method names that describe their function  

## Further testing
"Django includes `LiveServerTestCase` to facilitate integration with tools like Selenium."  
