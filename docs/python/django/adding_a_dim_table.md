# Add a size dim table to Vendor Studio API

## Contents


## Add a model

```python
# vsapi/studio/models.py
class Size(UserTimeStampModel):
    puid = models.UUIDField(default=uuid4, db_index=True)
    label = models.CharField(max_length=128, unique=True)

    class Meta:
        db_table = "size"
        verbose_name = "Size"
        verbose_name_plural = "Sizes"
        default_related_name = "sizes"
```

## Make migrations and migrate
To create a migration file, run `makemigrations`:
```shell
docker compose exec django-server python manage.py makemigrations
```
Rename the migration to something sensible, then add another migration to seed the database.

To apply the changes to the database, you can run this:
```shell
docker compose exec django-server python manage.py migrate
```
or if you want to re-seed from production first, then run this:
```shell
docker compose exec django-server python manage.py seed_local_db -db production
```

### Check the objects in the Django shell
If we enter the Django shell using `docker compose exec django-server python manage.py shell`, then we can see our new size objects.
```python
from studio.models import Size

print(Size.objects.all())
<QuerySet [<Size: Size object (1)>, <Size: Size object (2)>, <Size: Size object (3)>, <Size: Size object (4)>, <Size: Size object (5)>, <Size: Size object (6)>]>
```
That output isn't super helpful, but at least we have six objects there. To get more useful output, add a `__str__` method to the model:
```python
class Size(UserTimeStampModel):
    puid = models.UUIDField(default=uuid4, db_index=True)
    label = models.CharField(max_length=128, unique=True)

    class Meta:
        db_table = "size"
        verbose_name = "Size"
        verbose_name_plural = "Sizes"
        default_related_name = "sizes"

    def __str__(self):
        return f"{self.label}"
```
Exit and re-enter the Django shell, and then fetch and print the objects again:
```python
from studio.models import Size

print(Size.objects.all())
<QuerySet [<Size: 1-50>, <Size: 51-100>, <Size: 101-200>, <Size: 201-500>, <Size: 501-1000>, <Size: 1001+>]>
```
That's better!

## Add a serializer

## Add a view
Views refer to serializers.
9USferfL2zix6NM6MzYe
## Removing an explicit join table
```python
class Migration(migrations.Migration):

    dependencies = [
        ("studio", "0057_remove_size_puid"),
    ]

    operations = [
        migrations.RemoveField(
            model_name="AnimationLook",
            name="puid",
        ),
        migrations.SeparateDatabaseAndState(
            database_operations=[
                migrations.RunSQL(
                    sql=(
                        "ALTER TABLE studio_animation_look "
                        "RENAME COLUMN animation_look_id "
                        "TO animationlook_id;"
                    ),
                    reverse_sql=(
                        "ALTER TABLE studio_animation_look "
                        "RENAME COLUMN animationlook_id "
                        "TO animation_look_id;"
                    ),
                ),
            ],
            state_operations=[
                migrations.RemoveField(
                    model_name="studioanimationlook",
                    name="animation_look",
                ),
                migrations.RemoveField(
                    model_name="studioanimationlook",
                    name="studio",
                ),
                migrations.AlterField(
                    model_name="studio",
                    name="animation_looks",
                    field=models.ManyToManyField(
                        db_table="studio_animation_look", to="studio.animationlook"
                    ),
                ),
                migrations.DeleteModel(
                    name="StudioAnimationLook",
                ),
            ],
        ),
    ]
```

For custom intermediary join tables:
Create many-to-many relationships by creating instances of the intermediary model.
[Extra fields on many-to-many relationships](https://docs.djangoproject.com/en/4.1/topics/db/models/#extra-fields-on-many-to-many-relationships)
